var Authentication = {
    setAuthentication : function (auth) {
        localStorage.auth = JSON.stringify(auth);
        localStorage.token = auth.key;
        this.$store.state.auth = auth;
        return true;
    },
    deleteAuthentication : function (auth) {
        delete localStorage.auth;
        delete localStorage.token;
        this.$store.state.auth = {};
        return true;
    },
    checkAuthentication : function () {
        console.log(localStorage);
        if(localStorage.auth === undefined || localStorage.token === undefined){
            return false;
        } else {
            var auth = JSON.parse(localStorage.auth);
            this.$store.state.auth = auth;
            return true;
        }
    }
};

module.exports = Authentication;